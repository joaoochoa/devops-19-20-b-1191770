DEVOPS -  CA 3 - PARTE 1
===================

### 1. Análise, design e implementação dos requisitos
### 1.1 - Começar a criar uma VM conforme descrito, seguindo os passos da aula:

Vamos instalar a VirtualBox onde vamos criar uma máquina virtual onde vai correr o Ubuntu 18.04
 
        1- Download da VirtualBox 6.1    https://www.virtualbox.org/
        2- Foi criada uma nova VM no VirtualBox. New (DevOps Ubuntu)
        3- No menu principal do VirtualBox, ir a File -> Host Network Manager, criar um novo host-only network com o seguinte IP: 192.168.56.1.
        4- Nos settings na nova VM -> Network, definir o Adapter1 como NAT e o Adapter2 definir como Host-only-adapter
        5- Fazer o download do Ubunto 18.04 https://help.ubuntu.com/community/Installation/MinimalCD
        6- Ir novamente aos settings da nova VM -> Storage -> Controller:IDE e selecionar o disco 'mini.iso'.
        7- Iniciar a VM e instalar o 'mini.iso'.
        8- Instalação concluida, foi removido o disco, iniciou-se a VM e foi feito o log-in.

### 2. De seguida, foram executados os seguintes comandos:

        sudo apt install net-tools
        sudo nano /etc/netplan/01-netcfg.yaml
        
Após o comando anterior ser executado foi escrito o seguinte código
        
        network:
        version: 2
        renderer: network
         ethernets:
            enp0s3:
                dhcp4: yes
            enp0s8:
                addresses:
                - 192.168.56.5/24
---
        Notas:
        CTRL O (Gravar nano .yaml)
        CTRL x (Sair do nano .yaml)  
---
        
        sudo netplan apply
        sudo apt install openssh-server (Permite utilizar o SSH para abrir sessões em acesso remoto)
        sudo nano /etc/ssh/sshd_config
        
   Descomente a linha PasswordAuthentication yes
   
        sudo service ssh restart
        sudo apt install vsftpd
        sudo nano /etc/vsftpd.conf (Para editar o ficheiro de vsftpd)
   Descomentar a linha write_enable=YES
   Correr o comando:
   
        sudo service vsftpd restart 

### 3. Abrir o terminal do computador e insira o seguinte comando: 

    ssh joaoochoa@192.168.56.5

 Para instalar o Git na VM, utilizou-se o comando:

    sudo apt install git

 
 Para instalar o Java, utilizou-se o comando:
 
    sudo apt install openjdk-8-jdk-headless
   
### 4. Clonar o seu repositório individual dentro da VM

Para concluir este passo foi executado o seguinte comando no terminal:

    git clone https://joaoochoa@bitbucket.org/joaoochoa/devops-19-20-b-1191770.git

### 5. Você deve tentar criar e executar o projeto 

Foi corrido o seguinte comando dentro da pasta do projeto na VM para instalar o maven:
    
    sudo apt-get install maven
    chmod +x mvnw		(para dar permissões de execução) 
  
Verificar se a aplicação está a correr.

    ./mvnw spring-boot:run na pasta /basic
 
Ir ao seguinte link, http://192.168.56.5:8080/ para confirmar que estava a funcionar.

   
### 6. Para instalar o gradle na VM foi corrido o seguinte comando:

     sudo apt install gradle
     chmod +x gradlew		(para dar permissões de execução) 
           
Verificar se a aplicação está a correr, mas mudei para o diretório CA2-> part1.1/gradle_basic_demo  
    
    chmod +x gradlew
    ./gradlew build 
       
Nota: (BUILD SUCCESSFUL)        
### 7. Conclusão

Todas as alterações do processo descrito acima foram confirmadas no repositório. Nesse contexto, uma tag foi
adicionada, e os arquivos foram confirmados e enviados ao repositório com a tag ca3-part1:

       git commit -a -m "fix #12 add Readme.md ca3-part1"
       git push
       git tag ca3-part1
       git push origin ca3-part1