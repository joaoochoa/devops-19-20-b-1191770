DEVOPS -  CA 3 - PARTE 2
===================
# Vagrant Multi VM Demonstration for Spring Basic Tutorial Application
    O objetivo da Parte 2 é usar o Vagrant para configurar um ambiente virtual, para executar o aplicativo tutorial spring boot application, gradle "basic" version (developed in CA2, Part2)

### Análise, design e implementação dos requisitos

- Este projeto demonstra como configurar duas VMs para executar o aplicativo Tutorial Básico do Spring.

        VM da Web : Executa o aplicativo Web dentro do Tomcat8
        VM db : Executa a Base de dados H2 como um processo do servidor.

- Exigências

        Instale o Vagrant e o VirtualBox no seu computador.

- Depois foi feito o download do repositório indicado e seguindo os passos do ficheiro README desse repositório, foi executado o comando:

        vagrant up
    -----------------------
- Isso criará duas VMs vagantes (ou seja, "db" e "web"). A primeira execução levará algum tempo porque as duas máquinas serão provisionadas com vários pacotes de software.

No host, você pode abrir o aplicativo da web spring usando uma das seguintes opções:

- http://localhost:8080/demo-0.0.1-SNAPSHOT/
- http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/
	    
Você também pode abrir o console H2 usando um dos seguintes URLs:

- http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console
- http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/h2-console
	
Para a conexão, use: jdbc: h2: tcp: //192.168.33.11: 9092 /./ jpadb


### 1. Foi primeiramente criada a pasta CA3 -> part2 e o respetivo ficheiro README.md e enviado para o repositório.
	
	git commit -m -a "fix #12 add Readme.md"
    git push 

### 2. Instalação Vagrant
       
Fazer download do software conforme o sistema operativo:
       
               https://www.vagrantup.com/downloads.html
       
   Instalar o Vagrant.
       
   Confirmar que a instalação foi efetuada com sucesso, ir à linha de comandos e escreve o seguinte comando:
       
           vagrant -v

### 3. Copie o ficheiro Vagrantfile para o seu repositório (dentro da pasta criada CA3 -> part2).

Para completar este passo foi necessário copiar o ficheiro **Vagrantfile** contido no download do repositório indicado https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/src/master/ e colocado dentro da pasta para este exercicio (CA3-part2).

### 4. Atualize a configuração do Vagrantfile para usar o seu próprio gradle version of the spring application.

Foi necessário colocar o repositório público devido a um problema de autenticação (password).
Foram feitas as seguinte alterações na parte final do ficheiro Vagrantfile:

Mudei o seguinte comando para clonar o meu próprio repositório:

      git clone https://joaoochoa@bitbucket.org/joaoochoa/devops-19-20-b-1191770.git
      cd devops-19-20-b-1191770/ca2/part2-ca3/demo
      chmod +x gradlew
      ./gradlew clean build
      # To deploy the war file to tomcat8 do the following command:
      sudo cp build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
    SHELL
### 5. Verifique em https://bitbucket.org/atb/tut-basic-gradle para ver as alterações necessárias para que o aplicativo spring use o servidor H2 na VM db. Para Replicar as alterações.
Foi inicialmente criada uma cópia da part2 do CA2 dentro da pasta CA2(part2-ca3) onde seriam replicadas todas as alterações necessárias.

**Alterações necessárias foram as seguintes:**

#### Committed fb3f026 - Added changed ca2-part2-ca3 for ca3
.idea/misc.xml (MODIFIED)

    +  <component name="JavaScriptSettings">
    +    <option name="languageLevel" value="JSX" />
    +  </component>

ca2/part2 -ca3/demo/build.gradle (MODIFIED)

    +	id 'war'
    +	providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
    
ca2/part2 -ca3/demo/src/main/java/com/greglturnquist/payroll/Employee.java (MODIFIED)

    +import java.util.Objects;
     
ca2/part2 -ca3/demo/src/main/java/com/greglturnquist/payroll/ServletInitializer.java (ADDED)

    +package com.greglturnquist.payroll;
    +
    +import org.springframework.boot.builder.SpringApplicationBuilder;
    +import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
    +
    +public class ServletInitializer extends SpringBootServletInitializer {
    +
    +	@Override
    +    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    +		        return application.sources(ReactAndSpringDataRestApplication.class);
    +	}
    +
    +}
    
 ca2/part2 -ca3/demo/src/main/js/app.js (MODIFIED)
 
        }
     
     	componentDidMount() { // <2>
    -		client({method: 'GET', path: '/api/employees'}).done(response => {
    +		client({method: 'GET', path: '/demo-0.0.1-SNAPSHOT/api/employees'}).done(response => {
     			this.setState({employees: response.entity._embedded.employees});
     		});
     	}   
    
    
ca2/part2 -ca3/demo/src/main/resources/application.properties (MODIFIED)

    -spring.data.rest.base-path=/api
    +server.servlet.context-path=/demo-0.0.1-SNAPSHOT
    +spring.data.rest.base-path=/api
    +spring.datasource.url=jdbc:h2:mem:jpadb
    +spring.datasource.driverClassName=org.h2.Driver
    +spring.datasource.username=sa
    +spring.datasource.password=
    +spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
    +spring.h2.console.enabled=true
    +spring.h2.console.path=/h2-console
    +spring.h2.console.settings.web-allow-others=true

#### Committed 928a837 - UPDATE: Fixes ref to css in index.html
ca2/part2 -ca3/demo/src/main/resources/templates/index.html (MODIFIED)

    <head lang="en">
         <meta charset="UTF-8"/>
         <title>ReactJS + Spring Data REST</title>
    -    <link rel="stylesheet" href="/main.css" />
    +    <link rel="stylesheet" href="main.css" />
     </head>
     <body>

#### Committed 9491de4 - ADD: Settings for remote h2 database.
ca2/part2 -ca3/demo/src/main/resources/application.properties MODIFIED

    server.servlet.context-path=/demo-0.0.1-SNAPSHOT
     spring.data.rest.base-path=/api
    -spring.datasource.url=jdbc:h2:mem:jpadb
    +#spring.datasource.url=jdbc:h2:mem:jpadb
    +spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/~/jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
     spring.datasource.driverClassName=org.h2.Driver
     spring.datasource.username=sa
     spring.datasource.password=

#### Committed e222855 - UPDATE: Fixes h2 connection string
ca2/part2 -ca3/demo/src/main/resources/application.properties (MODIFIED)
    
     server.servlet.context-path=/demo-0.0.1-SNAPSHOT
     spring.data.rest.base-path=/api
     #spring.datasource.url=jdbc:h2:mem:jpadb
    -spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/~/jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
    +# In the following settings the h2 file is created in /home/vagrant folder
    +spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
     spring.datasource.driverClassName=org.h2.Driver
     spring.datasource.username=sa
     spring.datasource.password=
    
#### Committed 51319e8 - UPDATE: So that spring will no drop de database on every execution
ca2/part2 -ca3/demo/src/main/resources/application.properties (MODIFIED)
    
     spring.datasource.username=sa
     spring.datasource.password=
     spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
    +# So that spring will no drop de database on every execution.
    +spring.jpa.hibernate.ddl-auto=update
     spring.h2.console.enabled=true
     spring.h2.console.path=/h2-console
     spring.h2.console.settings.web-allow-others=true
     
#### Committed fd2ccf8 - Vagrantfile demo modified 
ca3/part2/Vagrantfile (MODIFIED)

      # Change the following command to clone your own repository!
             git clone https://joaoochoa@bitbucket.org/joaoochoa/devops-19-20-b-1191770.git
             cd devops-19-20-b-1191770/ca2/part2-ca3/demo
      +      chmod +x gradlew
             ./gradlew clean build
             # To deploy the war file to tomcat8 do the following command:
      -      sudo cp build/libs/basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
      +      sudo cp build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
           SHELL
       
         end 
     
#### Committed 50ef0a0 - modified ca2-part2-ca3
ca2/ { part2 -ca3 → part2-ca3 } /demo/.gitignore (RENAMED)

(Tive um problema com um espaço part2 -ca3 onde foi feito RENAME sem o espaço part2-ca3)


### 5. Estando estas alterações concluídas, foi feito commit para o repositório.

Para verificar se tudo estava a correr:

Execute o comando: **vagrant up** no terminal para serem inicializadas as VMs de db e web e acederam-se aos links:

No host, você pode abrir o aplicativo da web spring usando uma das seguintes opções:

- http://localhost:8080/demo-0.0.1-SNAPSHOT/
- http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/
	    
Você também pode abrir o console H2 usando um dos seguintes URLs:

- http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console
- http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/h2-console
	
Para a conexão, use: jdbc: h2: tcp: //192.168.33.11: 9092 /./ jpadb

### 6. Conclusão
Todas as alterações do processo descrito acima foram confirmadas no repositório, web e db. Nesse contexto, uma tag foi
adicionada, e os arquivos foram confirmados e enviados ao repositório com a tag ca3-part2:

    git commit -a -m "fix #13 add Readme.md ca3-part3 final"
    git push
    git tag ca3-part2
    git push origin ca3-part2