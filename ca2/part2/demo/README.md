DEVOPS -  CA 2 - PARTE 2
===================


### 1. Análise, design e implementação dos requisitos
### 1.1 - Criar um novo branch chamado tut-basic-gradle, executando o seguinte código:

        git branch tut-basic-gradle
        git checkout tut-basic-gradle
        git push --set-upstream origin tut-basic-gradle

### 2. Crie um novo projeto Gradle
Conforme instruções no ficheiro Readme do tutorial utilizando o link seguinte:

https://start.spring.io

Passo 1 - Seleccionar: Project - Gradle Project

Passo 2 - Adicionar as seguintes dependências:

        Rest Repositories
        Thymeleaf
        JPA
        H2
Passo 3 - Seleccionar Java 8

Passo 4 - Concluir com *Generate* para criar o novo projeto Gradle


### 3. Extraia o arquivo zip gerado dentro da pasta "CA2 / Part2 /"

Pode verifiquar as *tasks* gradle disponíveis executando o seguinte código: 

    gradlew tasks


### 4. Apagar a pasta src 
   
   Apagar a pasta src do projeto de Gradle criado, uma vez que queremos usar o código do basic tutorial.
   
### 5. Introdução do código do projeto basic no projeto Gradle criado

Passo 1 - Copiar o pasta src do projeto basic para o projeto Gradle.

Passo 2 - Copiar o ficheiro webpack.config.js do projeto basic para o projeto Gradle.

Passo 3 - Copiar o ficheiro package.json do projeto basic para o projeto Gradle.

Passo 4 - Apagar a pasta src/main/resources/static/built/ do projeto Gradle.

### 6. Correr a aplicação executando o seguinte código:

    gradlew bootRun

Verificar se a aplicação está a correr.
 
Ir ao seguinte link,  http://localhost:8080  (Deve aparecer o nome do projeto, mas sem contéudo visto que o gradle não tem ainda plugin para o frontend).
   
### 7. Adicionar o plugin gradle para o frontend  
No ficheiro build.gradle adicionar o seguinte código à secção de plugins:

    plugins {
           	id "org.siouan.frontend" version "1.4.1"
           }
   
   
### 8. Adicionar também o plugin para o frontend
No ficheiro build.gradle adicionar o seguinte bloco de código:

    frontend {
        	nodeVersion = "12.13.1"
        	assembleScript = "run webpack"
        }
    
### 9. Atualizar o ficheiro package.json para configurar a execução do webpack

No ficheiro package.son, adicionar a seguinte linha de código ao bloco de scripts:
  
           "scripts": {
            "webpack": "webpack"
            },
    
### 10. Correr a aplicação executando o seguinte código:

       gradlew build
       gradlew bootRun

### 11. Adicionar tarefa para copiar o jar gerado para a pasta *dist* localizada na root do projeto

No ficheiro build.gradle adicionar o seguinte bloco de código:

       task jarCopy(type: Copy) {
                from 'build/libs/demo-0.0.1-SNAPSHOT.jar'
                into 'dist'
            }

Para executar esta tarefa, correr o seguinte comando na linha de comandos:

       gradlew jarCopy

### 12. Adicionar tarefa para apagar os ficheiros gerados pelo webpack. Esta tarefa deve ser realizada antes da tarefa *clean* 
(geralmente localizado em src / resources / main / static / built /)

No ficheiro build.gradle adicionar o seguinte bloco de código:

       clean.doFirst {
       delete 'src/main/resources/static/built/.'
       }
Para executar esta tarefa, correr o seguinte comando na linha de comandos:

       gradlew clean

### 13. Fazer merge do branch com o master

       git commit -a -m "Fix #10 add ca2-par2"
       git push   
       git checkout master
       git merge tut-basic-gradle
       git push

### 14. Conclusão

Todas as alterações do processo descrito acima foram confirmadas no repositório. Nesse contexto, uma tag foi
adicionada, e os arquivos foram confirmados e enviados ao repositório:

       git commit -a -m "fix #11 add Readme.md part2"
       git push
       git tag ca2-part2
       git push origin ca2-part2