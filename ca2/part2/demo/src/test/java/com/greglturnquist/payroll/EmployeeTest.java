package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class EmployeeTest {
    /**
     * Test for Employ Constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor - Happy case")
    void EmployeeConsstructorHappyCaseTest() {
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "trab");
        assertTrue(employee instanceof Employee);
    }

    @Test
    @DisplayName("Test for constructor - Happy case")
    void getFirstNameHappyCaseTest() {
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "trab");
        String expected = "Frodo";
        String result = employee.getFirstName();
        assertEquals(expected, result);
    }
}