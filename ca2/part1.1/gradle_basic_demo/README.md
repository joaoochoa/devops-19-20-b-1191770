Gradle Basic Demo
===================

Este é um aplicativo de demonstração que implementa um servidor básico de sala de bate-papo com vários segmentos.

O servidor suporta vários clientes simultâneos por meio de multithreading. Quando um cliente se conecta, o servidor solicita um nome de tela e continua solicitando um nome até que um único seja recebido. Depois que um cliente envia um nome exclusivo, o servidor o reconhece. Todas as mensagens desse cliente serão transmitidas para todos os outros clientes que enviaram um nome de tela exclusivo. Um simples "protocolo de bate-papo" é usado para gerir o registro / saída e transmissão de mensagens de um utilizador.


Pré-requisitos
-------------

 * Java JDK 8
 * Apache Log4J 2
 * Gradle 6.6 
   

Build
-----

Para criar um arquivo .jar com o aplicativo:

    gradlew build 

Execute o servidor
--------------

Abra um terminal e execute o seguinte comando no diretório raiz do projeto:

     java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001



Executar um cliente
------------

Abra outro terminal e execute a seguinte tarefa gradle no diretório raiz do projeto:

    gradlew runClient


A tarefa acima assume que o IP do servidor de bate-papo é "localhost" e sua porta é "59001".

Para executar vários clientes, basta abrir mais terminais e repetir a chamada da tarefa de classificação runClient

##CA2 - Parte 1

Objetivos e requisitos
------------
1. Faça o download e comprometa o aplicativo de exemplo no repositório pessoal.
2. Leia as instruções disponíveis no arquivo readme.md e experimente o aplicativo.
3. Adicione uma nova tarefa para executar o servidor.
4. Adicione um teste de unidade simples e atualize o script gradle para que ele possa executar o teste.
5. Adicione uma nova tarefa do tipo Copiar a ser usada para fazer um backup das fontes do aplicativo.
6. Adicione uma nova tarefa do tipo Zip a ser usada para criar um arquivo morto das fontes do aplicativo.
7. Adicione tag ca2-part1

### Análise, design e implementação dos requisitos
Essa atribuição foi baseada em uma demonstração básica do Gradle, portanto, os primeiros arquivos a serem carregados no repositório foram da demonstração.
Após o download do arquivo, todos os arquivos foram carregados no repositório:

    git commit -A -m "fix #4: add files ca2- part 1.1"
    git push

### Build
O próximo passo foi gerar um arquivo `.jar` do projeto com o Gradle, executando o seguinte comando:

    gradlew build
    
Este comando adicionou uma pasta de construção ao projeto com todos os arquivos do projeto: classes, distribuições, recursos,
scripts, arquivos temporários e também gerou um arquivo compactado (`.jar`) em build/ibs com todas essas informações.

### Execute o servidor

Para executar o servidor, o seguinte comando foi executado:

    java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
    
Em seguida, em um terminal separado, para executar o cliente, executamos:

    gradlew runClient
    
Após a mensagem de compilação bem-sucedida, uma janela de bate-papo é aberta.

A próxima tarefa abrange a criação para executar o servidor. Portanto, no arquivo build.gradle, uma tarefa com o seguinte código;

    task runClient(type:JavaExec, dependsOn: classes){
        group = "DevOps"
        description = "Launches a chat client that connects to a server on localhost:59001 "
  
        classpath = sourceSets.main.runtimeClasspath

        main = 'basic_demo.ChatClientApp'

        args 'localhost', '59001'
    }

Para verificar se tudo está perfeito após a adição do código mencionado:

   Execute o servidor usando o seguinte comando:
   
    gradlew executeServer
    
   Execute o servidor usand o seguinte comando:
   
    gradlew runClient
    

### Adicione testes de unidade e atualize o script Gradle

Para adicionar, adicione um teste de unidade ao aplicativo, a criação de uma classe de teste em _src / test / java / basic_demo / AppTest_.    

Nesta classe de teste, o código fornecido nos slides da classe foi adicionado.

    public class AppTest {
        @Test
        public void testAppHasAGreeting() {
        App classUnderTest = new App();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());
        }
    }


Como os testes de unidade exigem a junção 4.12 para serem executados, as dependências do projeto precisavam ser atualizadas para serem compatíveis.
com esta versão:

    dependencies {
        // Use Apache Log4J for logging
        implementation group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
        implementation group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
        testCompile group: 'junit', name: 'junit', version: '4.12'
    }

Depois de executar o teste de unidade com sucesso, essas alterações foram enviadas ao repositório.    
    
    git commit -A -m "fix #7 add test"
    git push

### Adicione uma nova tarefa do tipo Copy
Para criar um backup das fontes do aplicativo, uma tarefa `Copy` foi adicionada ao _build.gradle_: 

    task backup (type: Copy){
        from 'src'
        into 'backup'
    }
    

Esta tarefa copia todos os arquivos em _src_ para uma pasta chamada _backup_.

### Adicone uma nova tarefa do tipo Zip

Uma nova tarefa do tipo `Zip` foi adicionada ao _build.gradle_ para copiar o conteúdo da pasta _src_ para um novo ficheiro do tipo` .zip`
contido dentro de uma pasta zipfiles:

    task zipFile (type:Zip) {
        from 'src'
        archiveFileName = 'src.zip'
        destinationDirectory = file('zipfiles')
    }
    

### Conlusão

Todas as alterações do processo descrito acima foram confirmadas no repositório usando o Git. Nesse contexto, uma tag foi
adicionada, e os arquivos foram confirmados e enviados ao repositório:

    git commit -A -m "fix #8 add backup and ZipFile"
    git push

    git tag ca2-part1
    git push origin ca2-part1