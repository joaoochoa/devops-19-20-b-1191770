DEVOPS -  CA 5 - PART2
===================
### 1. Análise, design e implementação dos requisitos

## CI/CD Pipelines with Jenkins ##

### 1.1 Adição de testes unitários ao projeto desenvolvido no CA2 - Parte 2
 
 - Verifiquei que não tinha testes unitários foi por isso necessário criá-los: 
 
 
Adicionei dois testes unitários que são os seguintes: 


    public class EmployeeTest {
    /**
     * Test for Employ Constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor - Happy case")
    void EmployeeConsstructorHappyCaseTest() {
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "trab");
        assertTrue(employee instanceof Employee);
    }

    @Test
    @DisplayName("Test for constructor - Happy case")
    void getFirstNameHappyCaseTest() {
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "trab");
        String expected = "Frodo";
        String result = employee.getFirstName();
        assertEquals(expected, result);
    }
    }

- Fazer commit destas alterações ao meu projeto desenvolvido no ca2-part2.

		git commit -a -m "fix #17 add tests c5- part2"
		git push


### 2. Criação de Pipeline simplificada - Jenkins  

	Para a criação desta pipeline:

	- é necessário ir a *New Item*:
	- atribuir um nome para a pipeline (Ca5-part2)
	- seleccionar *Pipeline*
	- fazer OK.
	
	
Visto as stages *Checkout*, *Assemble*, *Test* e *Archive* serem iguais às do CA5-Part1.

Testei primeiro estas quatro stages para ver se estava a funcionar correctamente antes de tentar implementar as etapas *Javadoc* e *Publish Image*.

Pipeline script que cumpre os requisitos destas quatro etapas é seguinte:
 
PipelineScript:

    pipeline {
    agent any
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'joaoochoa-bitbucket', url: 'https://joaoochoa@bitbucket.org/joaoochoa/devops-19-20-b-1191770.git'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Building...'
                bat 'cd ca2/part2/demo & gradlew clean assemble'
            }
        }
        stage ('Test'){
            steps {
             echo 'Testing...'
             bat 'cd ca2/part2/demo & gradlew test'
             junit 'ca2/part2/demo/build/test-results/test/*.xml'
            }
        }


        stage('Archive') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'ca2/part2/demo/build/libs/*'
            }
        }

    }
}

### 3. Implementar a stage *Javadoc*.

Foi assim adicionado o seguinte código para implementar esta etapa:(Nota: instalação de plugin - opção Gerir o Jenkins - instalar HTML Publisher plugin) 

    stage ('Javadoc'){
             steps {
                echo 'Publishing...'
                bat 'cd ca2/part2/demo/ & gradlew javadoc'

                 publishHTML([
                             reportName: 'Javadoc',
                             reportDir: 'ca2/part2/demo/build/docs/javadoc/',
                             reportFiles: 'index.html',
                              keepAll: false,
                             alwaysLinkToLastBuild: false,
                             allowMissing: false
                              ])
            }
        }
        
### 4. Implementar a etapa *Publish Image*. 

    stage ('Publish Image') {
                  steps {
                      echo 'Publishing image...'
                       script {
                               docker.withRegistry('https://index.docker.io/v1/', 'joaoochoa-Docker') {
                                          def customImage =
                                          docker.build("joaoochoa/devops-19-20-b-1191770:${env.BUILD_ID}", "ca5/part2")
                                          customImage.push()
                               }
                       }
                  }
        }
        
Para correr esta etapa é necessário ter o Docker


 - Definir as credenciais de acesso ao DockerHub.
 
    Nota1:
	- executar docker 
	- docker info 
	
	Username: joaoochoa
	
	Registry:https://index.docker.io/v1/ 
	
	Nota2:
	
	- Na falta do Dockerfile para o projeto do ca2-part2
	
	- Coloquei na pasta ca5/part2 o dockerfile da parte web utilizado no Ca4. 

A criação da imagem do - container web - foi bem sucedida e a sua migração para o DockerHub também.

Corresponde à image a (tag 14), que foi a id de build que obtive sucesso nesta etapa (${env.BUILD_ID}).

    https://hub.docker.com/repository/docker/joaoochoa/devops-19-20-b-1191770
    
### 5. Jenkinsfile final 

Depois de várias tentativas, cheguei a esta versão final do script, que penso que cumpre todos os requisitos do enunciado

PipelineScript:

    pipeline {
    agent any
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'joaoochoa-bitbucket', url: 'https://joaoochoa@bitbucket.org/joaoochoa/devops-19-20-b-1191770.git'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Building...'
                bat 'cd ca2/part2/demo & gradlew clean assemble'
            }
        }
        stage ('Test'){
            steps {
             echo 'Testing...'
             bat 'cd ca2/part2/demo & gradlew test'
             junit 'ca2/part2/demo/build/test-results/test/*.xml'
            }
        }

        stage ('Javadoc'){
             steps {
                echo 'Publishing...'
                bat 'cd ca2/part2/demo/ & gradlew javadoc'

                 publishHTML([
                             reportName: 'Javadoc',
                             reportDir: 'ca2/part2/demo/build/docs/javadoc/',
                             reportFiles: 'index.html',
                              keepAll: false,
                             alwaysLinkToLastBuild: false,
                             allowMissing: false
                              ])
            }
        }

        stage('Archive') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'ca2/part2/demo/build/libs/*'
            }
        }
        stage ('Publish Image') {
            steps {
                 echo 'Publishing image...'
                 script {
						docker.withRegistry('https://index.docker.io/v1/', 'joaoochoa-Docker') {
							def customImage =
							docker.build("joaoochoa/devops-19-20-b-1191770:${env.BUILD_ID}", "ca5/part2")
                            customImage.push()
                            }
						}
            }
        }
    }
}   

### 6. Criação de Pipeline - Correr o Jenkinsfile através do Repositório Bitbucket

Na criação da pipeline é necessário:

Para a criação desta pipeline:

 - é necessário ir a *New Item*
 - escolher um nome para a pipeline (devops-Part2)
 - seleccionar *Pipeline* 
 - clickar em OK.


É agora necessário editar a página de configuração do *Job*.

Aqui é necessário seleccionar no campo Definition *Pipeline script from SCM* neste caso segunda opção.

No campo *SCM* seleccionar *Git*.

No campo *Repository URL* é necessário colocar o URL da nossa conta do Bitbucket, no meu caso:

      https://joaoochoa@bitbucket.org/joaoochoa/devops-19-20-b-1191770.git     

No campo *Credentials* é necessário seleccionar a credencial com o username e password da nossa conta de Bitbucket.

- Não alterei os restantes campos, 
- No campo *Script Path* é necessário colocar o *path* para o ficheiro Jenkinsfile, no meu caso:

        ca5/part2/Jenkinsfile
    
Depois isso é necessário *Save/Apply* as alterações e ir à Pipeline e fazer o *Build Now*.

A build foi bem sucedida, conseguindo fazer a build da pipeline através do ficheiro Jenkinsfile, localizado num repositório.


A criação da imagem do container web também foi bem sucedida, e a sua migração para o DockerHub também.

Corresponde à image a (tag 1), que foi a ID de build desta pipeline, em que foi realizada com sucesso na tarefa *Publish Image* (${env.BUILD_ID}).

### Explicação do script do ficheiro Jenkinsfile para as diferentes *Stages*

####  Checkout Stage

Para aceder ao código do repositório foi implementado o seguinte código:

     stages {
            stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git credentialsId: 'joaoochoa-bitbucket', url: 'https://joaoochoa@bitbucket.org/joaoochoa/devops-19-20-b-1191770.git'
                }
            }

#### Assemble Stage

Para fazer a compilação da aplicação e produzir os ficheiros de arquivo, foi implementado o seguinte código:

       stage('Assemble') {
                  steps {
                      echo 'Building...'
                      bat 'cd ca2/part2/demo & gradlew clean assemble'
                  }
              }
 
#### Test Stage

Para executar os testes unitários e publicar os seus resultados, foi implementado o seguinte código:

     stage ('Test'){
                     steps {
                         echo 'Testing...'
                         bat 'cd ca2/ca2_part2/demo & gradlew test'
                         junit 'ca2/ca2_part2/demo/build/test-results/test/*.xml'
                     }
                 }

### Javadoc stage

Para gerar o ficheiro javadoc do projeto e publicá-lo no jenkins foi necessário implementar o seguinte código:

    stage ('Javadoc'){
                 steps {
                    echo 'Publishing...'
                    bat 'cd ca2/part2/demo/ & gradlew javadoc'
    
                     publishHTML([
                                 reportName: 'Javadoc',
                                 reportDir: 'ca2/part2/demo/build/docs/javadoc/',
                                 reportFiles: 'index.html',
                                  keepAll: false,
                                 alwaysLinkToLastBuild: false,
                                 allowMissing: false
                                  ])
                }
            }

A tarefa *javadoc* produz os ficheiro *Javadoc* do projeto. O *publishHTML* permite armanezar as informação criadas nessa tarefa, no jenkins. 

Escolhi que a informação ficasse guardada no caminho ca2/part2/demo/build/docs/javadoc/ . 


#### Archive Stage

Para arquivar os ficheiros gerados durante a *Stage Assemble* no Jenkins, foi implementado o seguinte código:

      stage('Archive') {
                 steps {
                     echo 'Archiving...'
                     archiveArtifacts 'ca2/part2/demo/build/libs/*'
                 }
             }

O step archiveArtifacts permite seleccionar quais os ficheiros que se pretende arquivar.

### Publish Image Stage

Para gerar uma *Docker Image* com *Tomcat* com o *war file* e publicá-lo no *Docker Hub* foi implementado o seguinte código:

        stage ('Publish Image') {
                steps {
                    echo 'Publishing image...'
                        script {
        			    docker.withRegistry('https://index.docker.io/v1/', 'joaoochoa-Docker') {
        			    def customImage =
        			    docker.build("joaoochoa/devops-19-20-b-1191770:${env.BUILD_ID}", "ca5/part2")
                                customImage.push()
                                }
        		    }
                }
       
        
 - docker.withRegistry - permite que se faça o login à conta do DockerHub.
 
 - docker.build na primeira parte define para qual repositório se pretende enviar.
 
 - ${env.BUILD_ID}", - é a tag associada neste caso ao (ID da build no jenkins), na segunda parte é definido o local onde está o Dockerfile ("ca5/part2")
 
 - O customImage.push(), faz o envio da imagem criada para o DockerHub.

### 7. Conclusão
Todas as alterações do processo descrito acima foram confirmadas no repositório e no Jenkins.

Nesse contexto, foi enviado o Readme.md e a tag ca5-part2 para o repositório:

    git commit -a -m "fix #21 add Readme.md ca5-part2"
    git push
    git tag ca5-part2
    git push origin ca5-part2