DEVOPS -  CA 5 - PART1
===================
### 1. Análise, design e implementação dos requisitos
  
CI/CD Pipelines with Jenkins 


- Instalação Jenkins

Ir ao seguinte link:

        https://www.jenkins.io/doc/book/installing/#war-file
		
Para fazer o download, War file do Jenkins.		

Abrir um terminal da linha de comandos e mudar de directório para o qual se fez o download.

no meu caso: 

    C:\Users\BMW\Desktop>

Correr o seguinte comando na linha de comandos:

    java -jar jenkins.war
    
No terminal aparece uma password que tem que ser utilizada para fazer o setup da conta Jenkins.

Ir a http://localhost:8080, utilizar a password referida anteriormente, para criar a conta no Jenkins.

Seleccionei  a opção de instalar os plugins sugeridos.

Criei a primeira conta de administrador com o utilizador João Filipe Gonçalves Ochôa com o User ID - joaoochoa

### 2. Criar Credenciais 

Ir a Credentials e seleccionar global.

No campo Username é necessário colocar ao username da conta do Bitbucket.

A password solicitada é a da conta do Bitbucket.

O campo ID vai ser utilizado para o Jenkins poder aceder a um repositório, no meu caso escolhi:

    joaoochoa-bitbucket
    
No campo Description utilizei:

    joaoochoa-bitbucket
    
Esta credencial será posteriormente utilizada no ficheiro Jenkinsfile.

### 3. Criação de Pipeline simplificada - Jenkins 

Para a criação desta pipeline:

 - é necessário ir a *New Item*:
 - atribuir um nome para a pipeline (Ca5-joaoOchoa)
 - seleccionar *Pipeline*
 - fazer OK.

É agora necessário editar a página de configuração do *Job*.

Para termos acesso ao editor de script (Linguagem Groovy) é necessário em - *Pipeline* - escolher no campo Definition *Pipeline script* (neste caso primeira opção).

Assim é possível experimentar directamente o script. 

Para isso é necessário *Save/Apply* as alterações e ir à Pipeline e fazer o *Build Now*.

- O Script que foi editado foi o seguinte:


PipelineScript:

    pipeline {
    agent any
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'joaoochoa-bitbucket', url: 'https://joaoochoa@bitbucket.org/joaoochoa/devops-19-20-b-1191770.git'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Building...'
                bat 'cd ca2/part1.1/gradle_basic_demo & gradlew clean assemble'
            }
        }
        stage ('Test'){
            steps {
             echo 'Testing...'
             bat 'cd ca2/part1.1/gradle_basic_demo & gradlew test'
             junit 'ca2/part1.1/gradle_basic_demo/build/test-results/test/*.xml'
            }
        }


        stage('Archive') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'ca2/part1.1/gradle_basic_demo/build/distributions/*'
            }
        }



    }
}

Nota (1ª Build Now deu erro - não estava a seleccionar o caminho correto (Windows Batch Script -- cd ca2/part1/gradle_basic_demo & gradlew clean assemble))

### 4. Criação de Pipeline - Correr o Jenkinsfile através do Repositório 

Para a criação desta pipeline:

 - é necessário ir a *New Item*
 - escolher um nome para a pipeline (devops)
 - seleccionar *Pipeline* 
 - clickar em OK.

É agora necessário editar a página de configuração do *Job*.

Aqui é necessário seleccionar no campo Definition - *Pipeline script from SCM* - (neste caso segunda opção).

No campo *SCM* seleccionar *Git*.

No campo *Repository URL* é necessário colocar o URL da nossa conta do Bitbucket, no meu caso:

    https://joaoochoa@bitbucket.org/joaoochoa/devops-19-20-b-1191770.git    

No campo *Credentials* é necessário seleccionar a credencial com o username e password da nossa conta de Bitbucket.

- Não alterei os restantes campos
- No campo *Script Path* é necessário colocar o *path* para o ficheiro Jenkinsfile, no meu caso:

    ca5/part1/Jenkinsfile
    
De seguida é necessário *Save/Apply* as alterações e ir à Pipeline e fazer o *Build Now*.

A build foi bem sucedida, conseguindo fazer a build da pipeline através do ficheiro Jenkinsfile, localizado num repositório.



### 5. Explicação do script do ficheiro Jenkinsfile para as diferentes *Stages*

#### 5.1 Checkout Stage

Para aceder ao código do repositório foi implementado o seguinte código:

    stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git credentialsId: 'joaoochoa-bitbucket', url: 'https://joaoochoa@bitbucket.org/joaoochoa/devops-19-20-b-1191770.git'
                }
            }

####  5.2 Assemble Stage

Para fazer a compilação da aplicação e produzir os ficheiros de arquivo, foi implementado o seguinte código:

      stage('Assemble') {
                steps {
                    echo 'Building...'
                    bat 'cd ca2/part1.1/gradle_basic_demo & gradlew clean assemble'
                }
            }

####  5.3 Test Stage

Para executar os testes unitários e publicar os seus resultados, foi implementado o seguinte código:

     stage ('Test'){
                steps {
                    echo 'Testing...'
                    bat 'cd ca2/part1.1/gradle_basic_demo & gradlew test'
					junit 'ca2/part1.1/gradle_basic_demo/build/test-results/test/*.xml'
                }
            }

####  5.4 Archive Stage

Para arquivar os ficheiros gerados durante a *Stage Assemble* no Jenkins, foi implementado o seguinte código:

     stage('Archive') {
                steps {
                    echo 'Archiving...'
					archiveArtifacts 'ca2/part1.1/gradle_basic_demo/build/distributions/*'
                }
            }

O step archiveArtifacts permite seleccionar quais os ficheiros que se pretende arquivar.


### 6. Conclusão
Todas as alterações do processo descrito acima foram confirmadas no repositório e no Jenkins.

Nesse contexto, foi enviado o Readme.md e a tag ca5-part1 para o repositório

    git commit -a -m "fix #20 add Readme.md ca5-part1"
    git push
    git tag ca5-part1
    git push origin ca5-part1


