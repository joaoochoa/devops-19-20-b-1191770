package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    /**
     *
     * Exception with invalid Email - null case
     */
    @Test
    @DisplayName("Ensure IllegalArgumentException with null Email")
    void setemailNullTest() {
        // ARRANGE
        //ACT
        //ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
                new Employee("joao","ochoa","aluno","trab",null);
            });
        }

    /**
     *
     * Exception with invalid Email - Empty case
     */
    @Test
    @DisplayName("Ensure IllegalArgumentException with isEmpty Email")
    void setIsEmptyEmailTest() {
        // ARRANGE
        //ACT
        //ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("joao","ochoa","aluno","trab","");
        });
    }

    /**
     *
     * Exception with invalid firstName - null case
     */
    @Test
    @DisplayName("Ensure IllegalArgumentException with null firtName")
    void setFirstNameNulltest() {
        // ARRANGE
        //ACT
        //ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee(null,"ochoa","aluno","trab","joao@sapo.pt");
        });
    }

    /**
     *
     * Exception with invalid firstName - Empty case
     */
    @Test
    @DisplayName("Ensure IllegalArgumentException with Empty firtName")
    void setFirstNameEmptytest() {
        // ARRANGE
        //ACT
        //ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("","ochoa","aluno","trab","joao@sapo.pt");
        });
    }

    /**
     *
     * Exception with invalid lastName - null case
     */
    @Test
    @DisplayName("Ensure IllegalArgumentException with null LastName")
    void setLastNameNulltest() {
        // ARRANGE
        //ACT
        //ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("joao",null,"aluno","trab","joao@sapo.pt");
        });
    }

    /**
     *
     * Exception with invalid LastName - Empty case
     */
    @Test
    @DisplayName("Ensure IllegalArgumentException with Empty LastName")
    void setLastNameEmptytest() {
        // ARRANGE
        //ACT
        //ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("joao","","aluno","trab","joao@sapo.pt");
        });
    }

    /**
     *
     * Exception with invalid description - null case
     */
    @Test
    @DisplayName("Ensure IllegalArgumentException with null description")
    void setDescriptionNulltest() {
        // ARRANGE
        //ACT
        //ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("joao","ochoa",null,"trab","joao@sapo.pt");
        });
    }

    /**
     *
     * Exception with invalid  description - Empty case
     */
    @Test
    @DisplayName("Ensure IllegalArgumentException with Empty description")
    void setDescriptionEmptytest() {
        // ARRANGE
        //ACT
        //ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("joao","ochoa","","trab","joao@sapo.pt");
        });
    }

    /**
     *
     * Exception with invalid jobTitle - null case
     */
    @Test
    @DisplayName("Ensure IllegalArgumentException with null description")
    void setJobTitleNulltest() {
        // ARRANGE
        //ACT
        //ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("joao","ochoa","aluno",null,"joao@sapo.pt");
        });
    }

    /**
     *
     * Exception with invalid  jobTitle - Empty case
     */
    @Test
    @DisplayName("Ensure IllegalArgumentException with Empty JobTitle")
    void setJobTitleEmptytest() {
        // ARRANGE
        //ACT
        //ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("joao","ochoa","aluno","","joao@sapo.pt");
        });
    }

    /**
     *
     * Exception with invalid  email -
     */
    @Test
    @DisplayName("Ensure IllegalArgumentException with Invalid email")
    void setEmailinvalidTest() {
        // ARRANGE
        //ACT
        //ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("joao","ochoa","aluno","trab","joaosapo.pt");
        });
    }

}