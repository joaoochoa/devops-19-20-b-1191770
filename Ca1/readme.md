## 1. Análise, design e implementação com git

### Tag da versão inicial como v1.2.0.
Utilizar os comandos:

	git tag v1.2.0.
	
	git push origin V1.2.0.

### 2. Criar uma ramificação email-field
Utilizar o comando:

	git checkout -b email-field (Cria o novo branch e muda para esse branch).
## 2. Adicionar novo campo de email

   1º Ir à classe Employee e adicionar:
   
    - Atributo novo String email;
	- Construtor setEmail(email);
	- Método setEmail;
	   
   2º Ir à classe DataBaseLoader:
   
	- método public void run adicionar o novo string do email;

   3º Ir ao ficheiro app.js e adicionar:
   
	- Na class EmployeeList <th>Email</th>
	- Na class Employee <td>{this.props.employee.email}</td>
	
### 3. Na classe Employee nos métodos Set adicionar restrições para garantir que o atributo como input não pode ser null nem ter espaços vazios. Em caso inválido é lançada uma excepção.
	  
	  public void setEmail(String email){
		if (email != null && !email.isEmpty()) {
			this.email = email;
		} else {
			throw new IllegalArgumentException ("Input email is invalid");
		}
	 }

### 4. Criar classe de testes e fazer a devida validação dos métodos set.
   
	
    @Test
    @DisplayName("Ensure IllegalArgumentException with null Email")
    void setemailNullTest() {
        // ARRANGE
        //ACT
        //ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
                new Employee("joao","ochoa","aluno","trab",null);
            });
        }
	

	@Test
    @DisplayName("Ensure IllegalArgumentException with isEmpty Email")
    void setIsEmptyEmailTest() {
        // ARRANGE
        //ACT
        //ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("joao","ochoa","aluno","trab","");
        });
    }
	
### 4.  Servidor e cliente (Debug)

Com o comando (utilize o terminal):

	mvnw spring-boot:run
	
verificar se aplicação está funcional verificando se a tabela empregados é carregada na página
 
	localhost:8080  
   
### 5. Merge das alterações
Para finalização do suporte para branch email-field: 

Comandos (existência duma issues #2 (tarefa):

	git add.                   
    git commit -a -m “fix #2”      
    git push                       
    git checkout master            
    git merge email-field          
    git tag v1.3.0                  
    git push origin v1.3.0    
  
### 6. Criar uma ramificação fix-invalid-email
Utilizar o comando:

	git checkout -b fix-invalid-email (Cria o novo branch e muda para esse branch)
	
### 7. Na classe Employee no método setEmail garantir que o atributo como input a string contém o símbolo *@* e não pode ser null nem ter espaços vazios. Em caso inválido é lançada uma excepção.

	public void setEmail(String email){
		if ( email != null && !email.isEmpty() && email.contains("@")) {
			this.email = email;
		} else {
			throw new IllegalArgumentException ("Input email is invalid");
		}
	 }

### 7.1 Com o comando (utilize o terminal):

	mvnw spring-boot:run
	
verificar se aplicação está funcional verificando se a tabela empregados é carregada na página
 
	localhost:8080
	
### 8. Merge das alterações
Para finalização do suporte para branch fix-invalid-email: 	


Comandos (existência duma issues #2 email validação "@" (tarefa):

	git add.                   
    git commit -a -m “fix #2 email validação "@"      
    git push                       
    git checkout master            
    git merge fix-invalid-email         
    git tag v1.3.1                  
    git push origin v1.3.1   


### 9. Adicionar Tag ao repositório com ca1
Aquando da conclusão do presente relatório, os seguintes passos foram tomados 

Comandos (existência duma issues #5 (tarefa):

    git add.                      
    git commit -a -m “fix #5 execução do relatório final ca1 ficheiro readme.md”       
    git push                        
    git tag ca1                     
    git push origin ca1            
