DEVOPS -  CA 4
===================
### 1. Análise, design e implementação dos requisitos

- Instale o Docker Toolbox no seu computador.

   -https://docs.docker.com/toolbox/overview/
   
- O objetivo é usar o Docker para configurar um ambiente em container para
executar a nossa versão gradle.

- A primeira etapa foi fazer o download do exemplo relacionado ao docker, disponível em:

     -https://bitbucket.org/atb/docker-compose-spring-tut-demo/src/master/


   *Este exemplo é para ser adicionado à pasta CA4 do repositório*.

### 2. Modificar ficheiro DockerFile

Precisamos de editar o `DockerFile` dentro da pasta da web para o projeto ca2/part2-ca3.
 
- As modificações visam vincular no meu próprio repositório.

- Também é necessário conceder permissões de execução para gradlew.

- Modificar o nome do arquivo `.war` 

Onde é visível no seguinte código:

    FROM tomcat

    RUN apt-get update -y

    RUN apt-get install -f

    RUN apt-get install git -y

    RUN apt-get install nodejs -y

    RUN apt-get install npm -y

    RUN mkdir -p /tmp/build

    WORKDIR /tmp/build/

    RUN git clone https://joaoochoa@bitbucket.org/joaoochoa/devops-19-20-b-1191770.git

    WORKDIR /tmp/build/devops-19-20-b-1191770/ca2/part2-ca3/demo

    RUN chmod u+x gradlew

    RUN ./gradlew clean build

    RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

    EXPOSE 8080

### 3. Execute Build
Após concluir as alterações necessárias no DockerFile e instalar o Docker no PC, agora devemos executar,
dentro da pasta que possui o arquivo `docker-compose.yml`, o seguinte comando para executar a compilação:

    `docker-compose build`

Se tudo correr bem, executamos:

    `docker-compose up`

No host, agora podemos abrir o aplicativo da web Spring usando o seguinte URL :

-http://localhost:8080/demo-0.0.1-SNAPSHOT/

E também podemos abrir o console H2 usando o seguinte URL:

-http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console

Para fazer a conexão use:
 jdbc:h2:tcp://192.168.33.11:9092/./jpadb 


### 4. Publiquar as imagens db e web no DockerHub
Para enviar as imagens para o DockerHub, são necessárias várias etapas:

1. Faça login em https://hub.docker.com/
2. Clique em Criar Repositório.
3. Escolha um nome (por exemplo, joaoochoa/devops-19-20-b-1191770) e uma descrição "Ca4" para o seu repositório e clique em Criar.
4. No DockerHub na linha de comando, insira as suas credenciais quando solicitado, com:

    `docker login`
5. Verifique o ID da imagem usando

    `docker images`
6. Marque a imagem com a qual você deseja enviar:

    `docker tag <IMAGEID> joaoochoa / devops-19-20-b-1191770: tagname`
7. Envie as imagens para o repositório que criou:

    `docker push joaoochoa /devops-19-20-b-1191770: tagname` 

Depois destas etapas serem executadas, as imagens ficam acessíveis no DockerHub. 
Para o meu projeto em particular, elas ficam acessível no repositório `joaoochoa / devops-19-20-b-1191770`.  
As imagens com as tags `web` e `db`.

  - https://hub.docker.com/repository/docker/joaoochoa/devops-19-20-b-1191770/general

### 5. Commit confirmar e enviar a versão preliminar do CA4 
Após as etapas anteriores, a versão atual da pasta CA4 contém o DockerFile modificado

     git commit -m "fix #14: In the folder of the docker-compose.yml execute"
     git push


### 6. Usar um volume do container db para obter uma cópia do arquivo da Base de dados.

Execute:

        docker-compose exec db bash
        
de seguida:

        cp /usr/src/app/jpadb.mv.db /usr/src/data

(Nota) Analise se na pasta data contém um ficheiro jpadb.mv.db como desejado.

### 7. Commit do passo anterior: 

     git commit -m "fix #15: copy of the database file"
     git push

### 8. Conclusão

Todas as alterações do processo descrito acima foram confirmadas no repositório. Nesse contexto, uma tag foi
adicionada, e os arquivos foram confirmados e enviados ao repositório:

    git commit -a -m "fix #16 add Readme.md"
    git tag ca4
    git push origin ca4









